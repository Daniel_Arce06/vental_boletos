
package ClasesEntidad;


public class Asiento extends Cliente {
    
    int No_Asiento;
    int Precio;
    boolean Disponibilidad;
    String Zona;
    
    //CONSTRUCTOR
    public Asiento (int No_Asiento , int Precio , boolean Disponibilidad , String Zona){
        super("","","","",0,0,"",0);
        this.No_Asiento=No_Asiento;
        this.Precio=Precio;
        this.Disponibilidad=Disponibilidad;
        this.Zona=Zona;
    }
    
    //SETTERS
    
    public void setNo_Asiento(int No_Asiento){
        this.No_Asiento=No_Asiento;
    }
    
    public void setPrecio (int Precio){
        this.Precio=Precio;
    }
    
    public void setDisponibilidad(boolean Disponibilidad){
        this.Disponibilidad=Disponibilidad;
    }
    
    public void setZona (String Zona){
       this.Zona=Zona; 
    }
    
    //GETTERS
    
    public int getNo_Asiento(){
        return No_Asiento;
    }
    
    public int getPrecio(){
        return Precio;
    }
    
    public boolean getDisponibilidad(){
        return Disponibilidad;
    }
    
    public String getZona(){
        return Zona;
    }
    
    //METODOS
    
    public int Asiento_Disponible(boolean Disponible){
        if( Disponible == true){
            System.out.println("Asiento Disponible");
            return 1;
        }
        else
        {
            System.out.println("Asiento NO Disponible");
            return 0;
        }
    }
}
