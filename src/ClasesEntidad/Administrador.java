
package ClasesEntidad;

public class Administrador {
    
    String Nombre;
    int Edad;
    String Pswd;
    
    public Administrador(String Nombre , int Edad , String Pswd){
        
        this.Nombre=Nombre;
        this.Edad=Edad;
        this.Pswd=Pswd;
        
    }
    
    //SETTERS
    
    public void setNombre(String Nombre){
        this.Nombre=Nombre;
    }
    
    public void setEdad(int Edad){
        this.Edad=Edad;
    }
    
    public void setPswd (String Pswd){
        this.Pswd=Pswd;
    }
    
    //GETTERS
    
    public String getNombre(){
        return Nombre;
    }
    
    public int getEdad(){
        return Edad;
    }
    
    public String getPswd(){
        return Pswd;
    }
    
}
