
package ClasesEntidad;

public class Transporte {
    
    int Dia_Salida;
    int Mes_Salida;
    int Año_Salida;
    
    int Dia_Llegada;
    int Mes_Llegada;
    int Año_Llegada;
    
    int Hora_Salida;
    int Hora_Llegada;
    
    int Num_Asientos_Disponibles;
    
    String Fecha_Salida;
    String Fecha_Llegada;
    String Destino;
    
    //CONSTRUCTOR
    public Transporte(int Dia_Salida, int Mes_Salida , int Año_Salida , int Dia_Llegada , int Mes_Llegada , int Año_Llegada , int Hora_Salida , int Hora_Llegada , int Num_Asientos_Disponibles , String Fecha_Salida , String Fecha_Llegada , String Destino ){
        
        this.Dia_Salida=Dia_Salida;
        this.Mes_Salida=Mes_Salida;
        this.Año_Salida=Año_Salida;
        this.Dia_Llegada=Dia_Llegada;
        this.Mes_Llegada=Mes_Llegada;
        this.Año_Llegada=Año_Llegada;
        this.Hora_Salida=Hora_Salida;
        this.Hora_Llegada=Hora_Llegada;
        this.Num_Asientos_Disponibles=Num_Asientos_Disponibles;
        
        this.Fecha_Salida=Fecha_Salida;
        this.Fecha_Llegada=Fecha_Llegada;
        this.Destino=Destino;
          
    }
    
    //SETTERS 
    
    public void setDia_Salida (int Dia_Salida){
         this.Dia_Salida=Dia_Salida;  
    }
    
    public void setMes_Salida (int Mes_Salida){
        this.Mes_Salida=Mes_Salida;
    }
    
    public void setAño_Salida (int Año_Salida){
        this.Año_Salida=Año_Salida;
    }
    
    public void Dia_Llegada (int Dia_Llegada){
        this.Dia_Llegada=Dia_Llegada;
    }
    
    public void setMes_Llegada (int Mes_Llegada){
        this.Mes_Llegada=Mes_Llegada;
    }
    
    public void setAño_Llegada (int Año_Llegada){
        this.Año_Llegada=Año_Llegada; 
    }
    
    public void setHora_Salida (int Hora_Salida){
       this.Hora_Salida=Hora_Salida; 
    }
    
    public void setHora_Llegada (int Hora_Llegada){
        this.Hora_Llegada=Hora_Llegada;
    }
    
    
    public void setNum_Asientos_Disponibles (int Num_Asientos_Disponibles){
        this.Num_Asientos_Disponibles=Num_Asientos_Disponibles;
    }
    
    
    public void setFecha_Salida (String Fecha_Salida){
       this.Fecha_Salida=Fecha_Salida; 
    }
    
    public void setFecha_Llegada (String Fecha_Llegada){
        this.Fecha_Llegada=Fecha_Llegada;
    }
    
    public void setDestino (String Destino){
        this.Destino=Destino;
    }
    
    //GETTERS
    
    public int getDia_Salida(){
        return Dia_Salida;
    }
    
    public int getMes_Salida(){
        return Mes_Salida;
    }
    
    public int getAño_Salida(){
        return Año_Salida;
    }
    
    public int getDia_Llegada(){
        return Dia_Llegada;
    }
    
    public int getMes_Llegada(){
        return Mes_Llegada;
    }
    
    public int getAño_Llegada(){
        return Año_Llegada;
    }
    
    public int getHora_Salida(){
        return Hora_Salida;
    }
    
    public int getHora_Llegada(){
        return Hora_Llegada;
    }
    
    public int getNum_Asientos_Disponibles(){
        return Num_Asientos_Disponibles;
    }
    
    public String getFecha_Salida(){
        return Fecha_Salida;
    }
    
    public String getFecha_Llegada(){
        return Fecha_Llegada;
    }
    
    public String getDestino(){
        return Destino;
    }
    
}
