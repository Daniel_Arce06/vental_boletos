
package ClasesEntidad;


public class Empleado extends Administrador{
    
    int No_Empleado;
    int Antiguedad;
    int Total_Vendido;
    
    public Empleado(int No_Empleado , int Antiguedad , int Total_Vendido){
        
        super("",0,"");
        
        this.No_Empleado=No_Empleado;
        this.Antiguedad=Antiguedad;
        this.Total_Vendido=Total_Vendido;
        
    }
    
    //SETTERS
    
    public void setNo_Empleado(int No_Empleado){
        this.No_Empleado=No_Empleado;
    }
    
    public void setAntiguedad(int Antiguedad){
        this.Antiguedad=Antiguedad;
    }
    
    public void setTotal_Vendido(int Total_Vendido){
        this.Total_Vendido=Total_Vendido;
    }
    
    //GETTERS
    
    public int getNo_Empleado(){
        return No_Empleado;
    }
    
    public int getAntiguedad(){
        return Antiguedad;
    }
    
    public int getTotal_Vendido(){
        return Total_Vendido;
    }
    
}
