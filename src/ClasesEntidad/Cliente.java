
package ClasesEntidad;

//import Archivos.Archivo_Cliente;
import Archivos.Archivo_Objetos;
import java.util.ArrayList;
import java.io.Serializable;


public class Cliente implements Serializable {
    
    String Nombre;
    String Apellidos;
    String Mail;
    double Edad;
    double Tarjeta_Credito;
    String Año_Vencimiento;
    double Cv;
    String Pswd;
    
    public ArrayList<Cliente> List_Cliente = new ArrayList<Cliente>();
    public ArrayList<Cliente> List_Cliente_C = new ArrayList<Cliente>();
   // Archivo_Cliente archivoCliente = new Archivo_Cliente("archivoCliente.ser");
    
    
    //CONSTRUCTOR
    public Cliente(String Nombre , String Apellidos , String Mail , String Pswd , double Edad , double Tarjeta_Credito , String Año_Vencimiento , double Cv){
        this.Nombre=Nombre;
        this.Apellidos=Apellidos;
        this.Mail=Mail;
        this.Pswd=Pswd;
        this.Edad=Edad;
        this.Tarjeta_Credito=Tarjeta_Credito;
        this.Año_Vencimiento=Año_Vencimiento;
        this.Cv=Cv;  
    }
    
    //SETTERS
    
    public void setNombre(String Nombre){
        this.Nombre=Nombre;
    }
    
    public void setApellidos(String Apellidos){
        this.Apellidos=Apellidos;
    }
    
    public void setMail(String Mail){
        this.Mail=Mail;
    }
    
    public void setPswd(String Pswd){
        this.Pswd=Pswd;
    }
    
    public void setEdad(double Edad){
        this.Edad=Edad;
    }
    
    public void setTarjeta_Credito(double Tarjeta_Credito){
        this.Tarjeta_Credito=Tarjeta_Credito;
    }
    
    public void setAño_Vencimiento(String Año_Vencimiento){
        this.Año_Vencimiento=Año_Vencimiento;
    }
    
    public void setCv(double Cv){
        this.Cv=Cv; 
    }
    
    //GETTERS
    
    public String getNombre (){
        return Nombre;
    }
    
    public String getApellidos (){
        return Apellidos;
    }
    
    public String getMail (){
        return Mail;
    }
    
    public String getPswd (){
        return Pswd;
    }
    
    public double getEdad (){
        return Edad;
    }
    
    public double getTarjeta_Credito (){
        return Tarjeta_Credito;
    }
    
    public String getAño_Vencimiento (){
        return Año_Vencimiento;
    }
    
    public double getCv (){
        return Cv;
    }
    
    //METODOS
    
    public int Registro(String PswdC , double Tarjeta_CreditoC  , double CvC){
        
        int lengthP = PswdC.length();
        String Cad_Tarjeta=Double.toString(Tarjeta_CreditoC);
        int lengthTC = Cad_Tarjeta.length();
        String Cad_Cv=Double.toString(CvC);
        int lengthCv=Cad_Cv.length();
        System.out.println(lengthP);
        System.out.println(lengthTC);
        System.out.println(lengthCv);
        if(lengthP > 5 && lengthTC == 16 &&  lengthCv == 6)
        {
            System.out.println("Registro Exitoso");
            return 1;
        }
        else
        {
            System.out.println("Error al Registrar");
            return 0;
        }
        
        
    }
    
    public int Inicio_Sesión(String MailIS , String PswdIS){
               
        Archivo_Objetos objetoArchCliente = new Archivo_Objetos("Cliente.ser");
        ArrayList <Cliente> ListaClienteCopia= new ArrayList<Cliente>();
        
        ListaClienteCopia=objetoArchCliente.Obtener_Cliente();
        
        int length=ListaClienteCopia.size();
        String PswdArchivo;
        String MailArchivo;
        int h=0;
        System.out.println("Tamaño = " + length);
        int i;
        
        for( i=0 ; i<length-1 ; i++){
                      
            if(MailIS.compareTo(ListaClienteCopia.get(i+1).getMail()) == 0){
                System.out.println("Usuarios Iguales" + (i+1));
                if(PswdIS.compareTo(ListaClienteCopia.get(i+1).getPswd()) == 0){
                    System.out.println("Contraseñas Iguales");
                    h=1;
                    break;
                }
            }
            else
            {
                h=0;
            }            
        }
         
        if(h==1)
        {
            System.out.println("Inicio de Sesión Correcto");
            return 1 ;
        }
        else
        {
            System.err.println("Error al Iniciar Sesión");
            return 0 ;
        }
    }
    
}
