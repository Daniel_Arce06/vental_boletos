
package ClasesEntidad;


public class Bus extends Transporte {
    
    String Modelo_Bus;
    String Linea;
    String Chofer;
    String Placa;
    String Central_Salida;
    
    public Bus(String Modelo_Bus,String Linea,String Chofer,String Placa,String Central_Salida){
        super(0,0,0,0,0,0,0,0,0,"","",""); //HERENCIA
        
        this.Modelo_Bus=Modelo_Bus;
        this.Linea=Linea;
        this.Chofer=Chofer;
        this.Placa=Placa;
        this.Central_Salida=Central_Salida;
    }
    
    //SETTERS
    
    public void setModelo_Bus(String Modelo_Bus){
        this.Modelo_Bus=Modelo_Bus;
    }
    
    public void setLinea(String Linea){
        this.Linea=Linea;
    }
    
    public void setChofer(String Chofer){
        this.Chofer=Chofer;   
    }
    
    public void setPlaca(String Placa){
        this.Placa=Placa;
    }
    
    public void setCentral_Salida(String Central_Salida){
        this.Central_Salida=Central_Salida;
    }
    
    //GETTERS
    
    public String getModelo_Bus(){
        return Modelo_Bus;
    }
    
    public String getLinea(){
        return Linea;
    }
    
    public String getChofer(){
        return Chofer;
    }
    
    public String getPlaca(){
        return Placa;
    }
    
    public String getCentral_Salida(){
        return Central_Salida;
    }
    
    
    
    
}
