
package ClasesEntidad;
import java.io.Serializable;



public class Avion extends Transporte implements Serializable{
    
    String Piloto;
    String Aerolinea;
    String Modelo_Avión;
    int Consumo_Alimento;
    
    Avion[] ObjetoAvion = new Avion[5];
    
    //CONSTRUTOR
    public Avion(String Piloto , String Aerolinea , String Modelo_Avión ,int Consumo_Alimento){
        super(0,0,0,0,0,0,0,0,0,"","",""); //HERENCIA
        
        this.Piloto=Piloto;
        this.Aerolinea=Aerolinea;
        this.Modelo_Avión=Modelo_Avión;
        this.Consumo_Alimento=Consumo_Alimento;
        
    }
    
    //SETTERS
    
    public void setPiloto(String Piloto){
        this.Piloto=Piloto;
    }
    
    public void setAerolinea (String Aerolinea){
        this.Aerolinea=Aerolinea;
    }
    
    public void setModelo_Avion(String Modelo_Avión){
        this.Modelo_Avión=Modelo_Avión;
    }
    
    public void  setConsumo_Alimento(int Consumo_Alimento){
        this.Consumo_Alimento=Consumo_Alimento;
    }
    
    //GETTERS
    
    String getPiloto(){
        return Piloto;
    }
    
    String getAerolinea(){
        return Aerolinea;
    }
    
    String getModelo_Avion(){
        return Modelo_Avión;
    }
    
    int getConsumo_Alimento(){
        return Consumo_Alimento;
    }
    
}
