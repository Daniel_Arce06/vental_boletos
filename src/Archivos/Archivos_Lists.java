
package Archivos;

import ClasesEntidad.Administrador;
import ClasesEntidad.Asiento;
import ClasesEntidad.Avion;
import ClasesEntidad.Bus;
import ClasesEntidad.Empleado;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Archivos_Lists {
    
     String Nombre;
    
    public Archivos_Lists(String Nombre){
        this.Nombre=Nombre;
    }
    
    public void setNombre(String Nombre){
        this.Nombre=Nombre;
    }
    
    //OBJETOS
    //METODOS PARA ADMINISTRADOR
    public void AlmacenarAdministrador(Administrador ObjAux1){
    try{
        FileOutputStream fileStream=new FileOutputStream(Nombre);
        ObjectOutputStream os=new ObjectOutputStream(fileStream);
        os.writeObject(ObjAux1);
        os.close();
    }
    catch(Exception ex){
        System.err.println(ex.getMessage());
    }
    }
    
    public Administrador ObtenerAdministrador(){
    Administrador uno=null;
    try{
        FileInputStream fileStream=new FileInputStream(Nombre);
        ObjectInputStream os=new ObjectInputStream(fileStream);
        uno=(Administrador)os.readObject();
        return uno;
    }
    catch(Exception ex){
        System.err.println(ex.getMessage());
        return null;
    }
    }
    
    
    //ARREGLOS DE OBJETOS
    //METODOS PARA ASIENTO
    public void AlmacenarAsiento(Asiento ObjAux1[]){
    try{
        FileOutputStream fileStream=new FileOutputStream(Nombre);
        ObjectOutputStream os=new ObjectOutputStream(fileStream);
        os.writeObject(ObjAux1);
        os.close();
    }
    catch(Exception ex){
        System.err.println("Error al guardar el objeto");
    }
    }
    
    public Asiento[] ObtenerAsiento(){
    Asiento uno[]=null;
    try{
        FileInputStream fileStream=new FileInputStream(Nombre);
        ObjectInputStream os=new ObjectInputStream(fileStream);
        uno=(Asiento[])os.readObject();
        os.close();
    }
    catch(Exception ex){
        System.err.println("Error al obtener el objeto");
    }
    return uno;
    }
    
    
    //METODOS AVION
    public void AlmacenarAvion(Avion ObjAux1[]){
    try{
        FileOutputStream fileStream=new FileOutputStream(Nombre);
        ObjectOutputStream os=new ObjectOutputStream(fileStream);
        os.writeObject(ObjAux1);
        os.close();
    }
    catch(Exception ex){
        System.err.println("Error al guardar el objeto");
    }
    }
    
    public Avion[] ObtenerAvion(){
    Avion uno[]=null;
    try{
        FileInputStream fileStream=new FileInputStream(Nombre);
        ObjectInputStream os=new ObjectInputStream(fileStream);
        uno=(Avion[])os.readObject();
        os.close();
    }
    catch(Exception ex){
        System.err.println("Error al obtener el objeto");
    }
    return uno;
    }
    
    
    //METODOS BUS
    public void AlmacenarBus(Bus ObjAux1[]){
    try{
        FileOutputStream fileStream=new FileOutputStream(Nombre);
        ObjectOutputStream os=new ObjectOutputStream(fileStream);
        os.writeObject(ObjAux1);
        os.close();
    }
    catch(Exception ex){
        System.err.println("Error al guardar el objeto");
    }
    }
    
    public Bus[] ObtenerBus(){
    Bus uno[]=null;
    try{
        FileInputStream fileStream=new FileInputStream(Nombre);
        ObjectInputStream os=new ObjectInputStream(fileStream);
        uno=(Bus[])os.readObject();
        os.close();
    }
    catch(Exception ex){
        System.err.println("Error al obtener el objeto");
    }
    return uno;
    }
    
    
    //METODOS EMPLEADO
    public void AlmacenarEmpleado(Empleado ObjAux1[]){
    try{
        FileOutputStream fileStream=new FileOutputStream(Nombre);
        ObjectOutputStream os=new ObjectOutputStream(fileStream);
        os.writeObject(ObjAux1);
        os.close();
    }
    catch(Exception ex){
        System.err.println("Error al guardar el objeto");
    }
    }
    
    public Empleado[] ObtenerEmpleado(){
    Empleado uno[]=null;
    try{
        FileInputStream fileStream=new FileInputStream(Nombre);
        ObjectInputStream os=new ObjectInputStream(fileStream);
        uno=(Empleado[])os.readObject();
        os.close();
    }
    catch(Exception ex){
        System.err.println("Error al obtener el objeto");
    }
    return uno;
    }
    
    
}
