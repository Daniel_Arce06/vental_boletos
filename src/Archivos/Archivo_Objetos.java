
package Archivos;

import ClasesEntidad.Cliente;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Archivo_Objetos implements Serializable{
    String nombre;
    public Archivo_Objetos(String nombre){
        this.nombre=nombre;   
    }
    
    
   public void Almacenar_Cliente(ArrayList<Cliente> AlmacenarObjeto){
        try{
            FileOutputStream fileStream=new FileOutputStream(nombre);
            ObjectOutputStream os=new ObjectOutputStream(fileStream);
            os.writeObject(AlmacenarObjeto);
            os.close();}
            catch(Exception ex){
                    System.err.println("Error al guardar objeto" + ex);
                    }
        
    }
   
   
   public ArrayList<Cliente>  Obtener_Cliente(/*ArrayList<Libro> ContarObjeto*/){
        ArrayList<Cliente> basura= new ArrayList<Cliente>();
        basura.add(new Cliente("","","","",0,0,"",0));
        try{
            FileInputStream fileStream=new FileInputStream(nombre);
            ObjectInputStream os=new ObjectInputStream(fileStream);
            ArrayList<Cliente> uno=(ArrayList<Cliente>)os.readObject();
            os.close();
            return uno;
            
        }
        catch(Exception ex){
            System.err.println("Error al obtener el objeto");
        }
        return basura;
    }
}
