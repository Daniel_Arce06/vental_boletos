
package vistas;

import Archivos.Archivo_Objetos;
import ClasesEntidad.Avion;
import ClasesEntidad.Bus;
import ClasesEntidad.Cliente;
import java.util.ArrayList;

public class EleccionTransporte extends javax.swing.JFrame {

    Cliente objCliente = new Cliente("" , "" , "", "" , 0 , 0 , "" , 0);
    SeleccionarDetallesAvion objDetAv = new SeleccionarDetallesAvion();
    SeleccionarDetallesBus objDetBus = new SeleccionarDetallesBus();
    ArrayList <Cliente> ListaClienteCopia= new ArrayList<Cliente>();
    Archivo_Objetos objetoArchCliente = new Archivo_Objetos("Cliente.ser");
    Avion[] objAvion=new Avion[40];
    Bus[] objBus=new Bus[40];
    
    public EleccionTransporte() {
        initComponents();
        setLocationRelativeTo(null);
        ListaClienteCopia=objetoArchCliente.Obtener_Cliente();
        
    }
     
    
    //METODO PARA LLENAR LOS OBJETOS DE REVISTAS
    public void LlenarObjetos_Avion()
    {
        //ACAPULCO
        objAvion[0].setFecha_Salida("");
        objAvion[0].setMes_Salida(12);
        objAvion[0].setHora_Salida(7);
        objAvion[0].setAño_Salida(2016);
        objAvion[0].setFecha_Llegada("");
        objAvion[0].setHora_Llegada(8);
        objAvion[0].setMes_Llegada(12);
        objAvion[0].setAño_Llegada(2016);
        objAvion[0].setDestino("Acapulco");
        objAvion[0].setConsumo_Alimento(0);
        objAvion[0].setNum_Asientos_Disponibles(10);
        objAvion[0].setPiloto("Gerardo Guzmán Flores");
        objAvion[0].setAerolinea("Mexicana");
        objAvion[0].setModelo_Avion("BOEING 787-800");
        
        objAvion[1].setFecha_Salida("");
        objAvion[1].setMes_Salida(12);
        objAvion[1].setHora_Salida(7);
        objAvion[1].setAño_Salida(2016);
        objAvion[1].setFecha_Llegada("");
        objAvion[1].setHora_Llegada(8);
        objAvion[1].setMes_Llegada(12);
        objAvion[1].setAño_Llegada(2016);
        objAvion[1].setDestino("Acapulco");
        objAvion[1].setConsumo_Alimento(0);
        objAvion[1].setNum_Asientos_Disponibles(10);
        objAvion[1].setPiloto("Felipe Lopez");
        objAvion[1].setAerolinea("Aeromexico");
        objAvion[1].setModelo_Avion("BOEING-772");
           
        objAvion[2].setFecha_Salida("");
        objAvion[2].setMes_Salida(12);
        objAvion[2].setHora_Salida(7);
        objAvion[2].setAño_Salida(2016);
        objAvion[2].setFecha_Llegada("");
        objAvion[2].setHora_Llegada(8);
        objAvion[2].setMes_Llegada(12);
        objAvion[2].setAño_Llegada(2016);
        objAvion[2].setDestino("Acapulco");
        objAvion[2].setConsumo_Alimento(0);
        objAvion[2].setNum_Asientos_Disponibles(10);
        objAvion[2].setPiloto("Brian Machaen");
        objAvion[2].setAerolinea("Interjet");
        objAvion[2].setModelo_Avion("Airbus-305");
        
        objAvion[3].setFecha_Salida("");
        objAvion[3].setMes_Salida(12);
        objAvion[3].setHora_Salida(7);
        objAvion[3].setAño_Salida(2016);
        objAvion[3].setFecha_Llegada("");
        objAvion[3].setHora_Llegada(8);
        objAvion[3].setMes_Llegada(12);
        objAvion[3].setAño_Llegada(2016);
        objAvion[3].setDestino("Acapulco");
        objAvion[3].setConsumo_Alimento(0);
        objAvion[3].setNum_Asientos_Disponibles(10);
        objAvion[3].setPiloto("Ruben Perez");
        objAvion[3].setAerolinea("VivaAerobus");
        objAvion[3].setModelo_Avion("Airbus-5");
        
        objAvion[4].setFecha_Salida("");
        objAvion[4].setMes_Salida(12);
        objAvion[4].setHora_Salida(7);
        objAvion[4].setAño_Salida(2016);
        objAvion[4].setFecha_Llegada("");
        objAvion[4].setHora_Llegada(8);
        objAvion[4].setMes_Llegada(12);
        objAvion[4].setAño_Llegada(2016);
        objAvion[4].setDestino("Acapulco");
        objAvion[4].setConsumo_Alimento(0);
        objAvion[4].setNum_Asientos_Disponibles(10);
        objAvion[4].setPiloto("Marlye Cruz");
        objAvion[4].setAerolinea("Volaris");
        objAvion[4].setModelo_Avion("BOEING-775");
        
        objAvion[5].setFecha_Salida("");
        objAvion[5].setMes_Salida(12);
        objAvion[5].setHora_Salida(7);
        objAvion[5].setAño_Salida(2016);
        objAvion[5].setFecha_Llegada("");
        objAvion[5].setHora_Llegada(8);
        objAvion[5].setMes_Llegada(12);
        objAvion[5].setAño_Llegada(2016);
        objAvion[5].setDestino("Acapulco");
        objAvion[5].setConsumo_Alimento(0);
        objAvion[5].setNum_Asientos_Disponibles(10);
        objAvion[5].setPiloto("Hector Duran");
        objAvion[5].setAerolinea("Mexicana");
        objAvion[5].setModelo_Avion("BOEING-777");
        
        objAvion[6].setFecha_Salida("");
        objAvion[6].setMes_Salida(12);
        objAvion[6].setHora_Salida(7);
        objAvion[6].setAño_Salida(2016);
        objAvion[6].setFecha_Llegada("");
        objAvion[6].setHora_Llegada(8);
        objAvion[6].setMes_Llegada(12);
        objAvion[6].setAño_Llegada(2016);
        objAvion[6].setDestino("Acapulco");
        objAvion[6].setConsumo_Alimento(0);
        objAvion[6].setNum_Asientos_Disponibles(10);
        objAvion[6].setPiloto("Bruno Ramirez");
        objAvion[6].setAerolinea("Interjet");
        objAvion[6].setModelo_Avion("Airbus-405");
        
        objAvion[7].setFecha_Salida("");
        objAvion[7].setMes_Salida(12);
        objAvion[7].setHora_Salida(7);
        objAvion[7].setAño_Salida(2016);
        objAvion[7].setFecha_Llegada("");
        objAvion[7].setHora_Llegada(8);
        objAvion[7].setMes_Llegada(12);
        objAvion[7].setAño_Llegada(2016);
        objAvion[7].setDestino("Acapulco");
        objAvion[7].setConsumo_Alimento(0);
        objAvion[7].setNum_Asientos_Disponibles(10);
        objAvion[7].setPiloto("Julian Gomez");
        objAvion[7].setAerolinea("VivaAerobus");
        objAvion[7].setModelo_Avion("Airbus-600");
        
        //CANCÚN
        objAvion[8].setFecha_Salida("");
        objAvion[8].setMes_Salida(12);
        objAvion[8].setHora_Salida(10);
        objAvion[8].setAño_Salida(2016);
        objAvion[8].setFecha_Llegada("");
        objAvion[8].setHora_Llegada(12);
        objAvion[8].setMes_Llegada(12);
        objAvion[8].setAño_Llegada(2016);
        objAvion[8].setDestino("Cancún");
        objAvion[8].setConsumo_Alimento(0);
        objAvion[8].setNum_Asientos_Disponibles(10);
        objAvion[8].setPiloto("Claudio Rodriguez");
        objAvion[8].setAerolinea("Mexicana");
        objAvion[8].setModelo_Avion("BOEING 356");
        
        objAvion[9].setFecha_Salida("");
        objAvion[9].setMes_Salida(12);
        objAvion[9].setHora_Salida(10);
        objAvion[9].setAño_Salida(2016);
        objAvion[9].setFecha_Llegada("");
        objAvion[9].setHora_Llegada(12);
        objAvion[9].setMes_Llegada(12);
        objAvion[9].setAño_Llegada(2016);
        objAvion[9].setDestino("Cancún");
        objAvion[9].setConsumo_Alimento(0);
        objAvion[9].setNum_Asientos_Disponibles(10);
        objAvion[9].setPiloto("Fernando Fernandez");
        objAvion[9].setAerolinea("Aeromexico");
        objAvion[9].setModelo_Avion("Airbus 758");
        
        objAvion[10].setFecha_Salida("");
        objAvion[10].setMes_Salida(12);
        objAvion[10].setHora_Salida(10);
        objAvion[10].setAño_Salida(2016);
        objAvion[10].setFecha_Llegada("");
        objAvion[10].setHora_Llegada(12);
        objAvion[10].setMes_Llegada(12);
        objAvion[10].setAño_Llegada(2016);
        objAvion[10].setDestino("Cancún");
        objAvion[10].setConsumo_Alimento(0);
        objAvion[10].setNum_Asientos_Disponibles(10);
        objAvion[10].setPiloto("Tonatiu Peralta");
        objAvion[10].setAerolinea("VivaAerobus");
        objAvion[10].setModelo_Avion("BOEING 787");
        
        objAvion[11].setFecha_Salida("");
        objAvion[11].setMes_Salida(12);
        objAvion[11].setHora_Salida(10);
        objAvion[11].setAño_Salida(2016);
        objAvion[11].setFecha_Llegada("");
        objAvion[11].setHora_Llegada(12);
        objAvion[11].setMes_Llegada(12);
        objAvion[11].setAño_Llegada(2016);
        objAvion[11].setDestino("Cancún");
        objAvion[11].setConsumo_Alimento(0);
        objAvion[11].setNum_Asientos_Disponibles(10);
        objAvion[11].setPiloto("Blanco Guzman");
        objAvion[11].setAerolinea("Interjet");
        objAvion[11].setModelo_Avion("Airbus 875");
        
        objAvion[12].setFecha_Salida("");
        objAvion[12].setMes_Salida(12);
        objAvion[12].setHora_Salida(10);
        objAvion[12].setAño_Salida(2016);
        objAvion[12].setFecha_Llegada("");
        objAvion[12].setHora_Llegada(12);
        objAvion[12].setMes_Llegada(12);
        objAvion[12].setAño_Llegada(2016);
        objAvion[12].setDestino("Cancún");
        objAvion[12].setConsumo_Alimento(0);
        objAvion[12].setNum_Asientos_Disponibles(10);
        objAvion[12].setPiloto("Victor Camerun");
        objAvion[12].setAerolinea("Volaris");
        objAvion[12].setModelo_Avion("Airbus 452");
        
        objAvion[13].setFecha_Salida("");
        objAvion[13].setMes_Salida(12);
        objAvion[13].setHora_Salida(10);
        objAvion[13].setAño_Salida(2016);
        objAvion[13].setFecha_Llegada("");
        objAvion[13].setHora_Llegada(12);
        objAvion[13].setMes_Llegada(12);
        objAvion[13].setAño_Llegada(2016);
        objAvion[13].setDestino("Cancún");
        objAvion[13].setConsumo_Alimento(0);
        objAvion[13].setNum_Asientos_Disponibles(10);
        objAvion[13].setPiloto("Kevin Hernan");
        objAvion[13].setAerolinea("Volaris");
        objAvion[13].setModelo_Avion("Airbus 891");
        
        objAvion[14].setFecha_Salida("");
        objAvion[14].setMes_Salida(12);
        objAvion[14].setHora_Salida(10);
        objAvion[14].setAño_Salida(2016);
        objAvion[14].setFecha_Llegada("");
        objAvion[14].setHora_Llegada(12);
        objAvion[14].setMes_Llegada(12);
        objAvion[14].setAño_Llegada(2016);
        objAvion[14].setDestino("Cancún");
        objAvion[14].setConsumo_Alimento(0);
        objAvion[14].setNum_Asientos_Disponibles(10);
        objAvion[14].setPiloto("Carlos Beltran");
        objAvion[14].setAerolinea("VivaaAerobus");
        objAvion[14].setModelo_Avion("Airbus 875");
        
        objAvion[15].setFecha_Salida("");
        objAvion[15].setMes_Salida(12);
        objAvion[15].setHora_Salida(10);
        objAvion[15].setAño_Salida(2016);
        objAvion[15].setFecha_Llegada("");
        objAvion[15].setHora_Llegada(12);
        objAvion[15].setMes_Llegada(12);
        objAvion[15].setAño_Llegada(2016);
        objAvion[15].setDestino("Cancún");
        objAvion[15].setConsumo_Alimento(0);
        objAvion[15].setNum_Asientos_Disponibles(10);
        objAvion[15].setPiloto("Samuel Loera");
        objAvion[15].setAerolinea("Aeromexico");
        objAvion[15].setModelo_Avion("BOEING 887");
        
         //GUADALAJARA
        objAvion[16].setFecha_Salida("");
        objAvion[16].setMes_Salida(12);
        objAvion[16].setHora_Salida(15);
        objAvion[16].setAño_Salida(2016);
        objAvion[16].setFecha_Llegada("");
        objAvion[16].setHora_Llegada(16);
        objAvion[16].setMes_Llegada(12);
        objAvion[16].setAño_Llegada(2016);
        objAvion[16].setDestino("Guadalajara");
        objAvion[16].setConsumo_Alimento(0);
        objAvion[16].setNum_Asientos_Disponibles(10);
        objAvion[16].setPiloto("Aaron Ramirez");
        objAvion[16].setAerolinea("Mexicana");
        objAvion[16].setModelo_Avion("BOEING 421");
        
        objAvion[17].setFecha_Salida("");
        objAvion[17].setMes_Salida(12);
        objAvion[17].setHora_Salida(15);
        objAvion[17].setAño_Salida(2016);
        objAvion[17].setFecha_Llegada("");
        objAvion[17].setHora_Llegada(16);
        objAvion[17].setMes_Llegada(12);
        objAvion[17].setAño_Llegada(2016);
        objAvion[17].setDestino("Guadalajara");
        objAvion[17].setConsumo_Alimento(0);
        objAvion[17].setNum_Asientos_Disponibles(10);
        objAvion[17].setPiloto("Ricardo Perez");
        objAvion[17].setAerolinea("VivaAerobus");
        objAvion[17].setModelo_Avion("BOEING 875");
        
        objAvion[18].setFecha_Salida("");
        objAvion[18].setMes_Salida(12);
        objAvion[18].setHora_Salida(15);
        objAvion[18].setAño_Salida(2016);
        objAvion[18].setFecha_Llegada("");
        objAvion[18].setHora_Llegada(16);
        objAvion[18].setMes_Llegada(12);
        objAvion[18].setAño_Llegada(2016);
        objAvion[18].setDestino("Guadalajara");
        objAvion[18].setConsumo_Alimento(0);
        objAvion[18].setNum_Asientos_Disponibles(10);
        objAvion[18].setPiloto("Gabriel Hernandez");
        objAvion[18].setAerolinea("Mexicana");
        objAvion[18].setModelo_Avion("Airbus 545");
        
        objAvion[19].setFecha_Salida("");
        objAvion[19].setMes_Salida(12);
        objAvion[19].setHora_Salida(15);
        objAvion[19].setAño_Salida(2016);
        objAvion[19].setFecha_Llegada("");
        objAvion[19].setHora_Llegada(16);
        objAvion[19].setMes_Llegada(12);
        objAvion[19].setAño_Llegada(2016);
        objAvion[19].setDestino("Guadalajara");
        objAvion[19].setConsumo_Alimento(0);
        objAvion[19].setNum_Asientos_Disponibles(10);
        objAvion[19].setPiloto("Loren Glar");
        objAvion[19].setAerolinea("Interjet");
        objAvion[19].setModelo_Avion("Airbus 542");
        
        objAvion[20].setFecha_Salida("");
        objAvion[20].setMes_Salida(12);
        objAvion[20].setHora_Salida(15);
        objAvion[20].setAño_Salida(2016);
        objAvion[20].setFecha_Llegada("");
        objAvion[20].setHora_Llegada(16);
        objAvion[20].setMes_Llegada(12);
        objAvion[20].setAño_Llegada(2016);
        objAvion[20].setDestino("Guadalajara");
        objAvion[20].setConsumo_Alimento(0);
        objAvion[20].setNum_Asientos_Disponibles(10);
        objAvion[20].setPiloto("Juan Flores");
        objAvion[20].setAerolinea("Aeromexico");
        objAvion[20].setModelo_Avion("BOEING 828");
        
        objAvion[21].setFecha_Salida("");
        objAvion[21].setMes_Salida(12);
        objAvion[21].setHora_Salida(15);
        objAvion[21].setAño_Salida(2016);
        objAvion[21].setFecha_Llegada("");
        objAvion[21].setHora_Llegada(16);
        objAvion[21].setMes_Llegada(12);
        objAvion[21].setAño_Llegada(2016);
        objAvion[21].setDestino("Guadalajara");
        objAvion[21].setConsumo_Alimento(0);
        objAvion[21].setNum_Asientos_Disponibles(10);
        objAvion[21].setPiloto("Zac Juarez");
        objAvion[21].setAerolinea("Volaris");
        objAvion[21].setModelo_Avion("Airbus 454");
        
        objAvion[22].setFecha_Salida("");
        objAvion[22].setMes_Salida(12);
        objAvion[22].setHora_Salida(15);
        objAvion[22].setAño_Salida(2016);
        objAvion[22].setFecha_Llegada("");
        objAvion[22].setHora_Llegada(16);
        objAvion[22].setMes_Llegada(12);
        objAvion[22].setAño_Llegada(2016);
        objAvion[22].setDestino("Guadalajara");
        objAvion[22].setConsumo_Alimento(0);
        objAvion[22].setNum_Asientos_Disponibles(10);
        objAvion[22].setPiloto("Pedro Cruz");
        objAvion[22].setAerolinea("Mexicana");
        objAvion[22].setModelo_Avion("Airbus 541");
        
        objAvion[23].setFecha_Salida("");
        objAvion[23].setMes_Salida(12);
        objAvion[23].setHora_Salida(15);
        objAvion[23].setAño_Salida(2016);
        objAvion[23].setFecha_Llegada("");
        objAvion[23].setHora_Llegada(16);
        objAvion[23].setMes_Llegada(12);
        objAvion[23].setAño_Llegada(2016);
        objAvion[23].setDestino("Guadalajara");
        objAvion[23].setConsumo_Alimento(0);
        objAvion[23].setNum_Asientos_Disponibles(10);
        objAvion[23].setPiloto("Yahir Tirado");
        objAvion[23].setAerolinea("Volaris");
        objAvion[23].setModelo_Avion("Airbus 142");
        
        //NEW ORLEANS
        objAvion[24].setFecha_Salida("");
        objAvion[24].setMes_Salida(12);
        objAvion[24].setHora_Salida(17);
        objAvion[24].setAño_Salida(2016);
        objAvion[24].setFecha_Llegada("");
        objAvion[24].setHora_Llegada(22);
        objAvion[24].setMes_Llegada(12);
        objAvion[24].setAño_Llegada(2016);
        objAvion[24].setDestino("New Orleans");
        objAvion[24].setConsumo_Alimento(0);
        objAvion[24].setNum_Asientos_Disponibles(10);
        objAvion[24].setPiloto("Lu Brady");
        objAvion[24].setAerolinea("American Airlines");
        objAvion[24].setModelo_Avion("Airbus 452");
        
        objAvion[25].setFecha_Salida("");
        objAvion[25].setMes_Salida(12);
        objAvion[25].setHora_Salida(17);
        objAvion[25].setAño_Salida(2016);
        objAvion[25].setFecha_Llegada("");
        objAvion[25].setHora_Llegada(22);
        objAvion[25].setMes_Llegada(12);
        objAvion[25].setAño_Llegada(2016);
        objAvion[25].setDestino("New Orleans");
        objAvion[25].setConsumo_Alimento(0);
        objAvion[25].setNum_Asientos_Disponibles(10);
        objAvion[25].setPiloto("Randy Moss");
        objAvion[25].setAerolinea("United Airlines");
        objAvion[25].setModelo_Avion("BOEING 515");
        
        objAvion[26].setFecha_Salida("");
        objAvion[26].setMes_Salida(12);
        objAvion[26].setHora_Salida(17);
        objAvion[26].setAño_Salida(2016);
        objAvion[26].setFecha_Llegada("");
        objAvion[26].setHora_Llegada(22);
        objAvion[26].setMes_Llegada(12);
        objAvion[26].setAño_Llegada(2016);
        objAvion[26].setDestino("New Orleans");
        objAvion[26].setConsumo_Alimento(0);
        objAvion[26].setNum_Asientos_Disponibles(10);
        objAvion[26].setPiloto("Rob Gronwsky");
        objAvion[26].setAerolinea("Air Canada");
        objAvion[26].setModelo_Avion("BOEING 777");
     
        objAvion[27].setFecha_Salida("");
        objAvion[27].setMes_Salida(12);
        objAvion[27].setHora_Salida(17);
        objAvion[27].setAño_Salida(2016);
        objAvion[27].setFecha_Llegada("");
        objAvion[27].setHora_Llegada(22);
        objAvion[27].setMes_Llegada(12);
        objAvion[27].setAño_Llegada(2016);
        objAvion[27].setDestino("New Orleans");
        objAvion[27].setConsumo_Alimento(0);
        objAvion[27].setNum_Asientos_Disponibles(10);
        objAvion[27].setPiloto("Bob Gonskowsky");
        objAvion[27].setAerolinea("Interjet");
        objAvion[27].setModelo_Avion("BOEING 452");
        
        objAvion[28].setFecha_Salida("");
        objAvion[28].setMes_Salida(12);
        objAvion[28].setHora_Salida(17);
        objAvion[28].setAño_Salida(2016);
        objAvion[28].setFecha_Llegada("");
        objAvion[28].setHora_Llegada(22);
        objAvion[28].setMes_Llegada(12);
        objAvion[28].setAño_Llegada(2016);
        objAvion[28].setDestino("New Orleans");
        objAvion[28].setConsumo_Alimento(0);
        objAvion[28].setNum_Asientos_Disponibles(10);
        objAvion[28].setPiloto("Flu Wesley");
        objAvion[28].setAerolinea("Aeromexico");
        objAvion[28].setModelo_Avion("BOEING 541");
        
        objAvion[29].setFecha_Salida("");
        objAvion[29].setMes_Salida(12);
        objAvion[29].setHora_Salida(17);
        objAvion[29].setAño_Salida(2016);
        objAvion[29].setFecha_Llegada("");
        objAvion[29].setHora_Llegada(22);
        objAvion[29].setMes_Llegada(12);
        objAvion[29].setAño_Llegada(2016);
        objAvion[29].setDestino("New Orleans");
        objAvion[29].setConsumo_Alimento(0);
        objAvion[29].setNum_Asientos_Disponibles(10);
        objAvion[29].setPiloto("Travis Potter");
        objAvion[29].setAerolinea("Mexicana");
        objAvion[29].setModelo_Avion("Airbus 456");
        
        objAvion[30].setFecha_Salida("");
        objAvion[30].setMes_Salida(12);
        objAvion[30].setHora_Salida(17);
        objAvion[30].setAño_Salida(2016);
        objAvion[30].setFecha_Llegada("");
        objAvion[30].setHora_Llegada(22);
        objAvion[30].setMes_Llegada(12);
        objAvion[30].setAño_Llegada(2016);
        objAvion[30].setDestino("New Orleans");
        objAvion[30].setConsumo_Alimento(0);
        objAvion[30].setNum_Asientos_Disponibles(10);
        objAvion[30].setPiloto("Aaron Rogetrs");
        objAvion[30].setAerolinea("AmericanAirlines");
        objAvion[30].setModelo_Avion("BOEING 451");
        
        objAvion[31].setFecha_Salida("");
        objAvion[31].setMes_Salida(12);
        objAvion[31].setHora_Salida(17);
        objAvion[31].setAño_Salida(2016);
        objAvion[31].setFecha_Llegada("");
        objAvion[31].setHora_Llegada(22);
        objAvion[31].setMes_Llegada(12);
        objAvion[31].setAño_Llegada(2016);
        objAvion[31].setDestino("New Orleans");
        objAvion[31].setConsumo_Alimento(0);
        objAvion[31].setNum_Asientos_Disponibles(10);
        objAvion[31].setPiloto("Joe Flacco");
        objAvion[31].setAerolinea("United Airlines");
        objAvion[31].setModelo_Avion("BOEING 451");
        
        //BOSTON
        objAvion[32].setFecha_Salida("");
        objAvion[32].setMes_Salida(12);
        objAvion[32].setHora_Salida(14);
        objAvion[32].setAño_Salida(2016);
        objAvion[32].setFecha_Llegada("");
        objAvion[32].setHora_Llegada(21);
        objAvion[32].setMes_Llegada(12);
        objAvion[32].setAño_Llegada(2016);
        objAvion[32].setDestino("Boston");
        objAvion[32].setConsumo_Alimento(0);
        objAvion[32].setNum_Asientos_Disponibles(10);
        objAvion[32].setPiloto("David Villa");
        objAvion[32].setAerolinea("United Airlines");
        objAvion[32].setModelo_Avion("BOEING 451");
        
        objAvion[33].setFecha_Salida("");
        objAvion[33].setMes_Salida(12);
        objAvion[33].setHora_Salida(14);
        objAvion[33].setAño_Salida(2016);
        objAvion[33].setFecha_Llegada("");
        objAvion[33].setHora_Llegada(21);
        objAvion[33].setMes_Llegada(12);
        objAvion[33].setAño_Llegada(2016);
        objAvion[33].setDestino("Boston");
        objAvion[33].setConsumo_Alimento(0);
        objAvion[33].setNum_Asientos_Disponibles(10);
        objAvion[33].setPiloto("Lu Mosley");
        objAvion[33].setAerolinea("American Airlines");
        objAvion[33].setModelo_Avion("Airbus 445");
        
        objAvion[34].setFecha_Salida("");
        objAvion[34].setMes_Salida(12);
        objAvion[34].setHora_Salida(14);
        objAvion[34].setAño_Salida(2016);
        objAvion[34].setFecha_Llegada("");
        objAvion[34].setHora_Llegada(21);
        objAvion[34].setMes_Llegada(12);
        objAvion[34].setAño_Llegada(2016);
        objAvion[34].setDestino("Boston");
        objAvion[34].setConsumo_Alimento(0);
        objAvion[34].setNum_Asientos_Disponibles(10);
        objAvion[34].setPiloto("Aaron Hernandez");
        objAvion[34].setAerolinea("AirCanada");
        objAvion[34].setModelo_Avion("BOEING 454");
        
        objAvion[35].setFecha_Salida("");
        objAvion[35].setMes_Salida(12);
        objAvion[35].setHora_Salida(14);
        objAvion[35].setAño_Salida(2016);
        objAvion[35].setFecha_Llegada("");
        objAvion[35].setHora_Llegada(21);
        objAvion[35].setMes_Llegada(12);
        objAvion[35].setAño_Llegada(2016);
        objAvion[35].setDestino("Boston");
        objAvion[35].setConsumo_Alimento(0);
        objAvion[35].setNum_Asientos_Disponibles(10);
        objAvion[35].setPiloto("Robert DeNiro");
        objAvion[35].setAerolinea("Interjet");
        objAvion[35].setModelo_Avion("Airbus 485");
        
        objAvion[36].setFecha_Salida("");
        objAvion[36].setMes_Salida(12);
        objAvion[36].setHora_Salida(14);
        objAvion[36].setAño_Salida(2016);
        objAvion[36].setFecha_Llegada("");
        objAvion[36].setHora_Llegada(21);
        objAvion[36].setMes_Llegada(12);
        objAvion[36].setAño_Llegada(2016);
        objAvion[36].setDestino("Boston");
        objAvion[36].setConsumo_Alimento(0);
        objAvion[36].setNum_Asientos_Disponibles(10);
        objAvion[36].setPiloto("Blur Pitt");
        objAvion[36].setAerolinea("Aeromexico");
        objAvion[36].setModelo_Avion("Airbus 452");
        
        objAvion[37].setFecha_Salida("");
        objAvion[37].setMes_Salida(12);
        objAvion[37].setHora_Salida(14);
        objAvion[37].setAño_Salida(2016);
        objAvion[37].setFecha_Llegada("");
        objAvion[37].setHora_Llegada(21);
        objAvion[37].setMes_Llegada(12);
        objAvion[37].setAño_Llegada(2016);
        objAvion[37].setDestino("Boston");
        objAvion[37].setConsumo_Alimento(0);
        objAvion[37].setNum_Asientos_Disponibles(10);
        objAvion[37].setPiloto("Gabriel Hernandez");
        objAvion[37].setAerolinea("United Airlines");
        objAvion[37].setModelo_Avion("BOEING 452");
        
        objAvion[38].setFecha_Salida("");
        objAvion[38].setMes_Salida(12);
        objAvion[38].setHora_Salida(14);
        objAvion[38].setAño_Salida(2016);
        objAvion[38].setFecha_Llegada("");
        objAvion[38].setHora_Llegada(21);
        objAvion[38].setMes_Llegada(12);
        objAvion[38].setAño_Llegada(2016);
        objAvion[38].setDestino("Boston");
        objAvion[38].setConsumo_Alimento(0);
        objAvion[38].setNum_Asientos_Disponibles(10);
        objAvion[38].setPiloto("Riber Brady");
        objAvion[38].setAerolinea("AmericanAirlines");
        objAvion[38].setModelo_Avion("Airbus 452");
        
        objAvion[39].setFecha_Salida("");
        objAvion[39].setMes_Salida(12);
        objAvion[39].setHora_Salida(14);
        objAvion[39].setAño_Salida(2016);
        objAvion[39].setFecha_Llegada("");
        objAvion[39].setHora_Llegada(21);
        objAvion[39].setMes_Llegada(12);
        objAvion[39].setAño_Llegada(2016);
        objAvion[39].setDestino("Boston");
        objAvion[39].setConsumo_Alimento(0);
        objAvion[39].setNum_Asientos_Disponibles(10);
        objAvion[39].setPiloto("Antonio Baxin ");
        objAvion[39].setAerolinea("American Airlines");
        objAvion[39].setModelo_Avion("BOEING 777");
    }

    
    public void LlenarObjetos_Bus()
    {
        
        //ACAPULCO
        objBus[0].setFecha_Salida("");
        objBus[0].setMes_Salida(12);
        objBus[0].setHora_Salida(5);
        objBus[0].setAño_Salida(2016);
        objBus[0].setFecha_Llegada("");
        objBus[0].setHora_Llegada(10);
        objBus[0].setMes_Llegada(12);
        objBus[0].setAño_Llegada(2016);
        objBus[0].setDestino("Acapulco");
        objBus[0].setNum_Asientos_Disponibles(5);
        objBus[0].setChofer("Braulio Terranz");
        objBus[0].setLinea("Primera Plus");
        objBus[0].setPlaca("SSS-555");
        objBus[0].setCentral_Salida("Central del Sur");
        objBus[0].setModelo_Bus("Volvo 7700");
   
        objBus[1].setFecha_Salida("");
        objBus[1].setMes_Salida(12);
        objBus[1].setHora_Salida(5);
        objBus[1].setAño_Salida(2016);
        objBus[1].setFecha_Llegada("");
        objBus[1].setHora_Llegada(10);
        objBus[1].setMes_Llegada(12);
        objBus[1].setAño_Llegada(2016);
        objBus[1].setDestino("Acapulco");
        objBus[1].setNum_Asientos_Disponibles(5);
        objBus[1].setChofer("Adolfo Camacho");
        objBus[1].setLinea("Futura");
        objBus[1].setPlaca("FFF-123");
        objBus[1].setCentral_Salida("Central del Sur");
        objBus[1].setModelo_Bus("Mercedes Benz MarcoPolo");
        
        objBus[2].setFecha_Salida("");
        objBus[2].setMes_Salida(12);
        objBus[2].setHora_Salida(5);
        objBus[2].setAño_Salida(2016);
        objBus[2].setFecha_Llegada("");
        objBus[2].setHora_Llegada(10);
        objBus[2].setMes_Llegada(12);
        objBus[2].setAño_Llegada(2016);
        objBus[2].setDestino("Acapulco");
        objBus[2].setNum_Asientos_Disponibles(5);
        objBus[2].setChofer("Mario Teran");
        objBus[2].setLinea("ADO");
        objBus[2].setPlaca("AAA-222");
        objBus[2].setCentral_Salida("Central del Sur");
        objBus[2].setModelo_Bus("Scania IRIZAR");
        
        objBus[3].setFecha_Salida("");
        objBus[3].setMes_Salida(12);
        objBus[3].setHora_Salida(5);
        objBus[3].setAño_Salida(2016);
        objBus[3].setFecha_Llegada("");
        objBus[3].setHora_Llegada(10);
        objBus[3].setMes_Llegada(12);
        objBus[3].setAño_Llegada(2016);
        objBus[3].setDestino("Acapulco");
        objBus[3].setNum_Asientos_Disponibles(5);
        objBus[3].setChofer("Roberto Millares");
        objBus[3].setLinea("Estrella Blanca");
        objBus[3].setPlaca("EEE-421");
        objBus[3].setCentral_Salida("Central del Sur");
        objBus[3].setModelo_Bus("Volvo 7700");
        
        objBus[4].setFecha_Salida("");
        objBus[4].setMes_Salida(12);
        objBus[4].setHora_Salida(5);
        objBus[4].setAño_Salida(2016);
        objBus[4].setFecha_Llegada("");
        objBus[4].setHora_Llegada(10);
        objBus[4].setMes_Llegada(12);
        objBus[4].setAño_Llegada(2016);
        objBus[4].setDestino("Acapulco");
        objBus[4].setNum_Asientos_Disponibles(5);
        objBus[4].setChofer("Diego Almazan");
        objBus[4].setLinea("ETN");
        objBus[4].setPlaca("ETT-989");
        objBus[4].setCentral_Salida("Central del Sur");
        objBus[4].setModelo_Bus("Scania IRIZAR");
        
        objBus[5].setFecha_Salida("");
        objBus[5].setMes_Salida(12);
        objBus[5].setHora_Salida(5);
        objBus[5].setAño_Salida(2016);
        objBus[5].setFecha_Llegada("");
        objBus[5].setHora_Llegada(10);
        objBus[5].setMes_Llegada(12);
        objBus[5].setAño_Llegada(2016);
        objBus[5].setDestino("Acapulco");
        objBus[5].setNum_Asientos_Disponibles(5);
        objBus[5].setChofer("Karumy Flores");
        objBus[5].setLinea("Primera Plus");
        objBus[5].setPlaca("PPP-666");
        objBus[5].setCentral_Salida("Central del Sur");
        objBus[5].setModelo_Bus("Mercedes Benz MarcoPolo");
        
        objBus[6].setFecha_Salida("");
        objBus[6].setMes_Salida(12);
        objBus[6].setHora_Salida(5);
        objBus[6].setAño_Salida(2016);
        objBus[6].setFecha_Llegada("");
        objBus[6].setHora_Llegada(10);
        objBus[6].setMes_Llegada(12);
        objBus[6].setAño_Llegada(2016);
        objBus[6].setDestino("Acapulco");
        objBus[6].setNum_Asientos_Disponibles(5);
        objBus[6].setChofer("Ruben Salazar");
        objBus[6].setLinea("Futura");
        objBus[6].setPlaca("FFF-532");
        objBus[6].setCentral_Salida("Central del Sur");
        objBus[6].setModelo_Bus("Volvo 7700");
        
        objBus[7].setFecha_Salida("");
        objBus[7].setMes_Salida(12);
        objBus[7].setHora_Salida(5);
        objBus[7].setAño_Salida(2016);
        objBus[7].setFecha_Llegada("");
        objBus[7].setHora_Llegada(10);
        objBus[7].setMes_Llegada(12);
        objBus[7].setAño_Llegada(2016);
        objBus[7].setDestino("Acapulco");
        objBus[7].setNum_Asientos_Disponibles(5);
        objBus[7].setChofer("Marlon Amador");
        objBus[7].setLinea("ETN");
        objBus[7].setPlaca("ETT-329");
        objBus[7].setCentral_Salida("Central del Sur");
        objBus[7].setModelo_Bus("Scania IRIZAR");
        
        //GUADALAJARA
        objBus[8].setFecha_Salida("");
        objBus[8].setMes_Salida(12);
        objBus[8].setHora_Salida(5);
        objBus[8].setAño_Salida(2016);
        objBus[8].setFecha_Llegada("");
        objBus[8].setHora_Llegada(11);
        objBus[8].setMes_Llegada(12);
        objBus[8].setAño_Llegada(2016);
        objBus[8].setDestino("Guadalajara");
        objBus[8].setNum_Asientos_Disponibles(5);
        objBus[8].setChofer("Oscar Rodriguez");
        objBus[8].setLinea("Estrella Blanca");
        objBus[8].setPlaca("EEE-541");
        objBus[8].setCentral_Salida("Central del Norte");
        objBus[8].setModelo_Bus("Mercedes Benz MarcoPolo");
        
        objBus[9].setFecha_Salida("");
        objBus[9].setMes_Salida(12);
        objBus[9].setHora_Salida(5);
        objBus[9].setAño_Salida(2016);
        objBus[9].setFecha_Llegada("");
        objBus[9].setHora_Llegada(11);
        objBus[9].setMes_Llegada(12);
        objBus[9].setAño_Llegada(2016);
        objBus[9].setDestino("Guadalajara");
        objBus[9].setNum_Asientos_Disponibles(5);
        objBus[9].setChofer("Miguel Ponce");
        objBus[9].setLinea("ETN");
        objBus[9].setPlaca("ETT-541");
        objBus[9].setCentral_Salida("Central del Norte");
        objBus[9].setModelo_Bus("Volvo 7700");
        
        objBus[10].setFecha_Salida("");
        objBus[10].setMes_Salida(12);
        objBus[10].setHora_Salida(5);
        objBus[10].setAño_Salida(2016);
        objBus[10].setFecha_Llegada("");
        objBus[10].setHora_Llegada(11);
        objBus[10].setMes_Llegada(12);
        objBus[10].setAño_Llegada(2016);
        objBus[10].setDestino("Guadalajara");
        objBus[10].setNum_Asientos_Disponibles(5);
        objBus[10].setChofer("Uriel Garcia");
        objBus[10].setLinea("ADO");
        objBus[10].setPlaca("AAA-541");
        objBus[10].setCentral_Salida("Central del Norte");
        objBus[10].setModelo_Bus("Scania IRIZAR");
        
        objBus[11].setFecha_Salida("");
        objBus[11].setMes_Salida(12);
        objBus[11].setHora_Salida(5);
        objBus[11].setAño_Salida(2016);
        objBus[11].setFecha_Llegada("");
        objBus[11].setHora_Llegada(11);
        objBus[11].setMes_Llegada(12);
        objBus[11].setAño_Llegada(2016);
        objBus[11].setDestino("Guadalajara");
        objBus[11].setNum_Asientos_Disponibles(5);
        objBus[11].setChofer("Ernesto Cornejo");
        objBus[11].setLinea("Primera Plus");
        objBus[11].setPlaca("PPP-311");
        objBus[11].setCentral_Salida("Central del Norte");
        objBus[11].setModelo_Bus("Mercedes Benz MarcoPolo");
        
        objBus[12].setFecha_Salida("");
        objBus[12].setMes_Salida(12);
        objBus[12].setHora_Salida(5);
        objBus[12].setAño_Salida(2016);
        objBus[12].setFecha_Llegada("");
        objBus[12].setHora_Llegada(11);
        objBus[12].setMes_Llegada(12);
        objBus[12].setAño_Llegada(2016);
        objBus[12].setDestino("Guadalajara");
        objBus[12].setNum_Asientos_Disponibles(5);
        objBus[12].setChofer("Enrique Sanchez");
        objBus[12].setLinea("Futura");
        objBus[12].setPlaca("FFF-976");
        objBus[12].setCentral_Salida("Central del Norte");
        objBus[12].setModelo_Bus("Volvo 7700");
        
        objBus[13].setFecha_Salida("");
        objBus[13].setMes_Salida(12);
        objBus[13].setHora_Salida(5);
        objBus[13].setAño_Salida(2016);
        objBus[13].setFecha_Llegada("");
        objBus[13].setHora_Llegada(11);
        objBus[13].setMes_Llegada(12);
        objBus[13].setAño_Llegada(2016);
        objBus[13].setDestino("Guadalajara");
        objBus[13].setNum_Asientos_Disponibles(5);
        objBus[13].setChofer("Luis Marin");
        objBus[13].setLinea("Estrella Blanca");
        objBus[13].setPlaca("EEE-542");
        objBus[13].setCentral_Salida("Central del Norte");
        objBus[13].setModelo_Bus("Scania IRIZAR");
        
        objBus[14].setFecha_Salida("");
        objBus[14].setMes_Salida(12);
        objBus[14].setHora_Salida(5);
        objBus[14].setAño_Salida(2016);
        objBus[14].setFecha_Llegada("");
        objBus[14].setHora_Llegada(11);
        objBus[14].setMes_Llegada(12);
        objBus[14].setAño_Llegada(2016);
        objBus[14].setDestino("Guadalajara");
        objBus[14].setNum_Asientos_Disponibles(5);
        objBus[14].setChofer("Jose Fonseca");
        objBus[14].setLinea("ETN");
        objBus[14].setPlaca("ETT-721");
        objBus[14].setCentral_Salida("Central del Norte");
        objBus[14].setModelo_Bus("Mercedes Benz MarcoPolo");
        
        objBus[15].setFecha_Salida("");
        objBus[15].setMes_Salida(12);
        objBus[15].setHora_Salida(5);
        objBus[15].setAño_Salida(2016);
        objBus[15].setFecha_Llegada("");
        objBus[15].setHora_Llegada(11);
        objBus[15].setMes_Llegada(12);
        objBus[15].setAño_Llegada(2016);
        objBus[15].setDestino("Guadalajara");
        objBus[15].setNum_Asientos_Disponibles(5);
        objBus[15].setChofer("David Tovar");
        objBus[15].setLinea("ADO");
        objBus[15].setPlaca("AAA-302");
        objBus[15].setCentral_Salida("Central del Norte");
        objBus[15].setModelo_Bus("Volvo 7700");
        
         //GUANAJUATO
        objBus[16].setFecha_Salida("");
        objBus[16].setMes_Salida(12);
        objBus[16].setHora_Salida(5);
        objBus[16].setAño_Salida(2016);
        objBus[16].setFecha_Llegada("");
        objBus[16].setHora_Llegada(12);
        objBus[16].setMes_Llegada(12);
        objBus[16].setAño_Llegada(2016);
        objBus[16].setDestino("Guanajuato");
        objBus[16].setNum_Asientos_Disponibles(5);
        objBus[16].setChofer("Alejandro Venancio");
        objBus[16].setLinea("Futura");
        objBus[16].setPlaca("FFF-220");
        objBus[16].setCentral_Salida("Central del Norte");
        objBus[16].setModelo_Bus("Scania IRIZAR");
        
        objBus[17].setFecha_Salida("");
        objBus[17].setMes_Salida(12);
        objBus[17].setHora_Salida(5);
        objBus[17].setAño_Salida(2016);
        objBus[17].setFecha_Llegada("");
        objBus[17].setHora_Llegada(12);
        objBus[17].setMes_Llegada(12);
        objBus[17].setAño_Llegada(2016);
        objBus[17].setDestino("Guanajuato");
        objBus[17].setNum_Asientos_Disponibles(5);
        objBus[17].setChofer("Michell Castro");
        objBus[17].setLinea("Estrella Blanca");
        objBus[17].setPlaca("EEE-559");
        objBus[17].setCentral_Salida("Central del Norte");
        objBus[17].setModelo_Bus("Mercedes Benz MarcoPolo");
        
        objBus[18].setFecha_Salida("");
        objBus[18].setMes_Salida(12);
        objBus[18].setHora_Salida(5);
        objBus[18].setAño_Salida(2016);
        objBus[18].setFecha_Llegada("");
        objBus[18].setHora_Llegada(12);
        objBus[18].setMes_Llegada(12);
        objBus[18].setAño_Llegada(2016);
        objBus[18].setDestino("Guanajuato");
        objBus[18].setNum_Asientos_Disponibles(5);
        objBus[18].setChofer("Juan Trujillo");
        objBus[18].setLinea("ETN");
        objBus[18].setPlaca("ETT-438");
        objBus[18].setCentral_Salida("Central del Norte");
        objBus[18].setModelo_Bus("Volvo 7700");
        
        objBus[19].setFecha_Salida("");
        objBus[19].setMes_Salida(12);
        objBus[19].setHora_Salida(5);
        objBus[19].setAño_Salida(2016);
        objBus[19].setFecha_Llegada("");
        objBus[19].setHora_Llegada(12);
        objBus[19].setMes_Llegada(12);
        objBus[19].setAño_Llegada(2016);
        objBus[19].setDestino("Guanajuato");
        objBus[19].setNum_Asientos_Disponibles(5);
        objBus[19].setChofer("Gabriel Mejia");
        objBus[19].setLinea("ADO");
        objBus[19].setPlaca("AAA-777");
        objBus[19].setCentral_Salida("Central del Norte");
        objBus[19].setModelo_Bus("Scania IRIZAR");
        
        objBus[20].setFecha_Salida("");
        objBus[20].setMes_Salida(12);
        objBus[20].setHora_Salida(5);
        objBus[20].setAño_Salida(2016);
        objBus[20].setFecha_Llegada("");
        objBus[20].setHora_Llegada(12);
        objBus[20].setMes_Llegada(12);
        objBus[20].setAño_Llegada(2016);
        objBus[20].setDestino("Guanajuato");
        objBus[20].setNum_Asientos_Disponibles(5);
        objBus[20].setChofer("Angel Gallardo");
        objBus[20].setLinea("Futura");
        objBus[20].setPlaca("FFF-777");
        objBus[20].setCentral_Salida("Central del Norte");
        objBus[20].setModelo_Bus("Mercedes Benz MarcoPolo");
        
        objBus[21].setFecha_Salida("");
        objBus[21].setMes_Salida(12);
        objBus[21].setHora_Salida(5);
        objBus[21].setAño_Salida(2016);
        objBus[21].setFecha_Llegada("");
        objBus[21].setHora_Llegada(12);
        objBus[21].setMes_Llegada(12);
        objBus[21].setAño_Llegada(2016);
        objBus[21].setDestino("Guanajuato");
        objBus[21].setNum_Asientos_Disponibles(5);
        objBus[21].setChofer("Bruno Diaz");
        objBus[21].setLinea("Estrella Blanca");
        objBus[21].setPlaca("EEE-881");
        objBus[21].setCentral_Salida("Central del Norte");
        objBus[21].setModelo_Bus("Volvo 7700");
        
        objBus[22].setFecha_Salida("");
        objBus[22].setMes_Salida(12);
        objBus[22].setHora_Salida(5);
        objBus[22].setAño_Salida(2016);
        objBus[22].setFecha_Llegada("");
        objBus[22].setHora_Llegada(12);
        objBus[22].setMes_Llegada(12);
        objBus[22].setAño_Llegada(2016);
        objBus[22].setDestino("Guanajuato");
        objBus[22].setNum_Asientos_Disponibles(5);
        objBus[22].setChofer("Bruno Diaz");
        objBus[22].setLinea("Estrella Blanca");
        objBus[22].setPlaca("EEE-881");
        objBus[22].setCentral_Salida("Central del Norte");
        objBus[22].setModelo_Bus("Volvo 7700");
        
        objBus[23].setFecha_Salida("");
        objBus[23].setMes_Salida(12);
        objBus[23].setHora_Salida(15);
        objBus[23].setAño_Salida(2016);
        objBus[23].setFecha_Llegada("");
        objBus[23].setHora_Llegada(16);
        objBus[23].setMes_Llegada(12);
        objBus[23].setAño_Llegada(2016);
        objBus[23].setDestino("Guadalajara");
        objBus[23].setConsumo_Alimento(0);
        objBus[23].setNum_Asientos_Disponibles(10);
        objBus[23].setPiloto("Yahir Tirado");
        objBus[23].setAerolinea("Volaris");
        objBus[23].setModelo_Avion("Airbus 142");
        
        //QUERETARO
        objBus[24].setFecha_Salida("");
        objBus[24].setMes_Salida(12);
        objBus[24].setHora_Salida(17);
        objBus[24].setAño_Salida(2016);
        objBus[24].setFecha_Llegada("");
        objBus[24].setHora_Llegada(22);
        objBus[24].setMes_Llegada(12);
        objBus[24].setAño_Llegada(2016);
        objBus[24].setDestino("New Orleans");
        objBus[24].setConsumo_Alimento(0);
        objBus[24].setNum_Asientos_Disponibles(10);
        objBus[24].setPiloto("Lu Brady");
        objBus[24].setAerolinea("American Airlines");
        objBus[24].setModelo_Avion("Airbus 452");
        
        objBus[25].setFecha_Salida("");
        objBus[25].setMes_Salida(12);
        objBus[25].setHora_Salida(17);
        objBus[25].setAño_Salida(2016);
        objBus[25].setFecha_Llegada("");
        objBus[25].setHora_Llegada(22);
        objBus[25].setMes_Llegada(12);
        objBus[25].setAño_Llegada(2016);
        objBus[25].setDestino("New Orleans");
        objBus[25].setConsumo_Alimento(0);
        objBus[25].setNum_Asientos_Disponibles(10);
        objBus[25].setPiloto("Randy Moss");
        objBus[25].setAerolinea("United Airlines");
        objBus[25].setModelo_Avion("BOEING 515");
        
        objBus[26].setFecha_Salida("");
        objBus[26].setMes_Salida(12);
        objBus[26].setHora_Salida(17);
        objBus[26].setAño_Salida(2016);
        objBus[26].setFecha_Llegada("");
        objBus[26].setHora_Llegada(22);
        objBus[26].setMes_Llegada(12);
        objBus[26].setAño_Llegada(2016);
        objBus[26].setDestino("New Orleans");
        objBus[26].setConsumo_Alimento(0);
        objBus[26].setNum_Asientos_Disponibles(10);
        objBus[26].setPiloto("Rob Gronwsky");
        objBus[26].setAerolinea("Air Canada");
        objBus[26].setModelo_Avion("BOEING 777");
     
        objBus[27].setFecha_Salida("");
        objBus[27].setMes_Salida(12);
        objBus[27].setHora_Salida(17);
        objBus[27].setAño_Salida(2016);
        objBus[27].setFecha_Llegada("");
        objBus[27].setHora_Llegada(22);
        objBus[27].setMes_Llegada(12);
        objBus[27].setAño_Llegada(2016);
        objBus[27].setDestino("New Orleans");
        objBus[27].setConsumo_Alimento(0);
        objBus[27].setNum_Asientos_Disponibles(10);
        objBus[27].setPiloto("Bob Gonskowsky");
        objBus[27].setAerolinea("Interjet");
        objBus[27].setModelo_Avion("BOEING 452");
        
        objBus[28].setFecha_Salida("");
        objBus[28].setMes_Salida(12);
        objBus[28].setHora_Salida(17);
        objBus[28].setAño_Salida(2016);
        objBus[28].setFecha_Llegada("");
        objBus[28].setHora_Llegada(22);
        objBus[28].setMes_Llegada(12);
        objBus[28].setAño_Llegada(2016);
        objBus[28].setDestino("New Orleans");
        objBus[28].setConsumo_Alimento(0);
        objBus[28].setNum_Asientos_Disponibles(10);
        objBus[28].setPiloto("Flu Wesley");
        objBus[28].setAerolinea("Aeromexico");
        objBus[28].setModelo_Avion("BOEING 541");
        
        objBus[29].setFecha_Salida("");
        objBus[29].setMes_Salida(12);
        objBus[29].setHora_Salida(17);
        objBus[29].setAño_Salida(2016);
        objBus[29].setFecha_Llegada("");
        objBus[29].setHora_Llegada(22);
        objBus[29].setMes_Llegada(12);
        objBus[29].setAño_Llegada(2016);
        objBus[29].setDestino("New Orleans");
        objBus[29].setConsumo_Alimento(0);
        objBus[29].setNum_Asientos_Disponibles(10);
        objBus[29].setPiloto("Travis Potter");
        objBus[29].setAerolinea("Mexicana");
        objBus[29].setModelo_Avion("Airbus 456");
        
        objBus[30].setFecha_Salida("");
        objBus[30].setMes_Salida(12);
        objBus[30].setHora_Salida(17);
        objBus[30].setAño_Salida(2016);
        objBus[30].setFecha_Llegada("");
        objBus[30].setHora_Llegada(22);
        objBus[30].setMes_Llegada(12);
        objBus[30].setAño_Llegada(2016);
        objBus[30].setDestino("New Orleans");
        objBus[30].setConsumo_Alimento(0);
        objBus[30].setNum_Asientos_Disponibles(10);
        objBus[30].setPiloto("Aaron Rogetrs");
        objBus[30].setAerolinea("AmericanAirlines");
        objBus[30].setModelo_Avion("BOEING 451");
        
        objBus[31].setFecha_Salida("");
        objBus[31].setMes_Salida(12);
        objBus[31].setHora_Salida(17);
        objBus[31].setAño_Salida(2016);
        objBus[31].setFecha_Llegada("");
        objBus[31].setHora_Llegada(22);
        objBus[31].setMes_Llegada(12);
        objBus[31].setAño_Llegada(2016);
        objBus[31].setDestino("New Orleans");
        objBus[31].setConsumo_Alimento(0);
        objBus[31].setNum_Asientos_Disponibles(10);
        objBus[31].setPiloto("Joe Flacco");
        objBus[31].setAerolinea("United Airlines");
        objBus[31].setModelo_Avion("BOEING 451");
        
        //TEOTIHUACAN
        objBus[32].setFecha_Salida("");
        objBus[32].setMes_Salida(12);
        objBus[32].setHora_Salida(14);
        objBus[32].setAño_Salida(2016);
        objBus[32].setFecha_Llegada("");
        objBus[32].setHora_Llegada(21);
        objBus[32].setMes_Llegada(12);
        objBus[32].setAño_Llegada(2016);
        objBus[32].setDestino("Boston");
        objBus[32].setConsumo_Alimento(0);
        objBus[32].setNum_Asientos_Disponibles(10);
        objBus[32].setPiloto("David Villa");
        objBus[32].setAerolinea("United Airlines");
        objBus[32].setModelo_Avion("BOEING 451");
        
        objBus[33].setFecha_Salida("");
        objBus[33].setMes_Salida(12);
        objBus[33].setHora_Salida(14);
        objBus[33].setAño_Salida(2016);
        objBus[33].setFecha_Llegada("");
        objBus[33].setHora_Llegada(21);
        objBus[33].setMes_Llegada(12);
        objBus[33].setAño_Llegada(2016);
        objBus[33].setDestino("Boston");
        objBus[33].setConsumo_Alimento(0);
        objBus[33].setNum_Asientos_Disponibles(10);
        objBus[33].setPiloto("Lu Mosley");
        objBus[33].setAerolinea("American Airlines");
        objBus[33].setModelo_Avion("Airbus 445");
        
        objBus[34].setFecha_Salida("");
        objBus[34].setMes_Salida(12);
        objBus[34].setHora_Salida(14);
        objBus[34].setAño_Salida(2016);
        objBus[34].setFecha_Llegada("");
        objBus[34].setHora_Llegada(21);
        objBus[34].setMes_Llegada(12);
        objBus[34].setAño_Llegada(2016);
        objBus[34].setDestino("Boston");
        objBus[34].setConsumo_Alimento(0);
        objBus[34].setNum_Asientos_Disponibles(10);
        objBus[34].setPiloto("Aaron Hernandez");
        objBus[34].setAerolinea("AirCanada");
        objBus[34].setModelo_Avion("BOEING 454");
        
        objBus[35].setFecha_Salida("");
        objBus[35].setMes_Salida(12);
        objBus[35].setHora_Salida(14);
        objBus[35].setAño_Salida(2016);
        objBus[35].setFecha_Llegada("");
        objBus[35].setHora_Llegada(21);
        objBus[35].setMes_Llegada(12);
        objBus[35].setAño_Llegada(2016);
        objBus[35].setDestino("Boston");
        objBus[35].setConsumo_Alimento(0);
        objBus[35].setNum_Asientos_Disponibles(10);
        objBus[35].setPiloto("Robert DeNiro");
        objBus[35].setAerolinea("Interjet");
        objBus[35].setModelo_Avion("Airbus 485");
        
        objBus[36].setFecha_Salida("");
        objBus[36].setMes_Salida(12);
        objBus[36].setHora_Salida(14);
        objBus[36].setAño_Salida(2016);
        objBus[36].setFecha_Llegada("");
        objBus[36].setHora_Llegada(21);
        objBus[36].setMes_Llegada(12);
        objBus[36].setAño_Llegada(2016);
        objBus[36].setDestino("Boston");
        objBus[36].setConsumo_Alimento(0);
        objBus[36].setNum_Asientos_Disponibles(10);
        objBus[36].setPiloto("Blur Pitt");
        objBus[36].setAerolinea("Aeromexico");
        objBus[36].setModelo_Avion("Airbus 452");
        
        objBus[37].setFecha_Salida("");
        objBus[37].setMes_Salida(12);
        objBus[37].setHora_Salida(14);
        objBus[37].setAño_Salida(2016);
        objBus[37].setFecha_Llegada("");
        objBus[37].setHora_Llegada(21);
        objBus[37].setMes_Llegada(12);
        objBus[37].setAño_Llegada(2016);
        objBus[37].setDestino("Boston");
        objBus[37].setConsumo_Alimento(0);
        objBus[37].setNum_Asientos_Disponibles(10);
        objBus[37].setPiloto("Gabriel Hernandez");
        objBus[37].setAerolinea("United Airlines");
        objBus[37].setModelo_Avion("BOEING 452");
        
        objBus[38].setFecha_Salida("");
        objBus[38].setMes_Salida(12);
        objBus[38].setHora_Salida(14);
        objBus[38].setAño_Salida(2016);
        objBus[38].setFecha_Llegada("");
        objBus[38].setHora_Llegada(21);
        objBus[38].setMes_Llegada(12);
        objBus[38].setAño_Llegada(2016);
        objBus[38].setDestino("Boston");
        objBus[38].setConsumo_Alimento(0);
        objBus[38].setNum_Asientos_Disponibles(10);
        objBus[38].setPiloto("Riber Brady");
        objBus[38].setAerolinea("AmericanAirlines");
        objBus[38].setModelo_Avion("Airbus 452");
        
        objBus[39].setFecha_Salida("");
        objBus[39].setMes_Salida(12);
        objBus[39].setHora_Salida(14);
        objBus[39].setAño_Salida(2016);
        objBus[39].setFecha_Llegada("");
        objBus[39].setHora_Llegada(21);
        objBus[39].setMes_Llegada(12);
        objBus[39].setAño_Llegada(2016);
        objBus[39].setDestino("Boston");
        objBus[39].setConsumo_Alimento(0);
        objBus[39].setNum_Asientos_Disponibles(10);
        objBus[39].setPiloto("Antonio Baxin ");
        objBus[39].setAerolinea("American Airlines");
        objBus[39].setModelo_Avion("BOEING 777");
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton4 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        Lbl_Nombre = new javax.swing.JLabel();
        Lbl_Temp = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        Btn_Vuelos = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        Btn_Bus = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(233, 233, 233));

        jPanel1.setBackground(new java.awt.Color(44, 95, 161));

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/usuario (1).png"))); // NOI18N
        jButton4.setBorder(null);
        jButton4.setBorderPainted(false);
        jButton4.setContentAreaFilled(false);

        jLabel5.setFont(new java.awt.Font("Arial Narrow", 0, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("BIENVENIDO");

        Lbl_Nombre.setFont(new java.awt.Font("Arial Narrow", 0, 14)); // NOI18N
        Lbl_Nombre.setForeground(new java.awt.Color(255, 255, 255));
        Lbl_Nombre.setText("NOMBRE_USUARIO");

        Lbl_Temp.setForeground(new java.awt.Color(255, 255, 255));
        Lbl_Temp.setText("1");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(Lbl_Nombre))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(Lbl_Temp)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel5)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton4)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jButton4)
                .addGap(0, 5, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(Lbl_Temp))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Lbl_Nombre)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.setBackground(new java.awt.Color(233, 233, 233));
        jLabel1.setFont(new java.awt.Font("Arial Narrow", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(135, 135, 135));
        jLabel1.setText("TURISMOCITY / VUELOS / AUTOBUSES");

        jPanel3.setBackground(new java.awt.Color(248, 137, 4));

        Btn_Vuelos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/avion.png"))); // NOI18N
        Btn_Vuelos.setBorder(null);
        Btn_Vuelos.setBorderPainted(false);
        Btn_Vuelos.setContentAreaFilled(false);
        Btn_Vuelos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn_VuelosActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Arial Narrow", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("VUELOS");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addGap(138, 138, 138))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Btn_Vuelos, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(38, Short.MAX_VALUE)
                .addComponent(Btn_Vuelos, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(118, 187, 0));

        Btn_Bus.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/autobus.png"))); // NOI18N
        Btn_Bus.setBorder(null);
        Btn_Bus.setBorderPainted(false);
        Btn_Bus.setContentAreaFilled(false);
        Btn_Bus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Btn_BusActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Arial Narrow", 0, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("AUTOBUSES");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(108, Short.MAX_VALUE)
                .addComponent(Btn_Bus)
                .addGap(103, 103, 103))
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(128, 128, 128)
                .addComponent(jLabel3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(76, 76, 76)
                .addComponent(Btn_Bus)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(25, 25, 25))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 740, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 58, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28))
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(64, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void Btn_VuelosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn_VuelosActionPerformed

        dispose();
        objDetAv.setVisible(true);   
        String x = Lbl_Temp.getText();
        System.out.println("Num: " + x);
        int h=Integer.parseInt(x);
        objDetAv.Lbl_Temp.setText(x);
        objDetAv.Lbl_Name.setText(ListaClienteCopia.get(h).getNombre());
        
    }//GEN-LAST:event_Btn_VuelosActionPerformed

    private void Btn_BusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Btn_BusActionPerformed
        dispose();
        objDetBus.setVisible(true);
        String x = Lbl_Temp.getText();
        System.out.println("Num: " + x);
        int h=Integer.parseInt(x);
        objDetBus.Lbl_Temp.setText(x);
        objDetBus.Lbl_Name.setText(ListaClienteCopia.get(h).getNombre());
    }//GEN-LAST:event_Btn_BusActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EleccionTransporte.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EleccionTransporte.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EleccionTransporte.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EleccionTransporte.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new EleccionTransporte().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Btn_Bus;
    private javax.swing.JButton Btn_Vuelos;
    public static javax.swing.JLabel Lbl_Nombre;
    public static javax.swing.JLabel Lbl_Temp;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    // End of variables declaration//GEN-END:variables
}
